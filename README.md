# Occam Web COVID Lab

This plugin adds the COVID Lab sub-website which is a place for experimentation
and aggregation of research strategies to investigate the properties of the
COVID-19 virus.

## Installation

You will need to run `npm install` and `npm run build` in this directory to
build the JavaScript required to run the plugin application.

## Models

To add a model to consideration within the COVID-Lab, the model should have the
"covid-lab-model" tag attached to it.

## Output

The output should be standardized.
