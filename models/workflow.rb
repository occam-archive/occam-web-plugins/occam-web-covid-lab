# A representation of our workflow.
class Occam
  module Plugins
    module CovidLab
      class Workflow
        def self.template
          {
            :connections => [
              {
                :inputs => [
                  {
                    :connections => [
                      {
                        :to => [
                          1,
                          -1,
                          0
                        ]
                      }
                    ],
                    :name => "<configuration name>",
                    :type => "configuration",
                    :visibility => "hidden"
                  }
                ],
                :position => {
                  :x => -142,
                  :y => -77
                },
                :name => "<model name>",
                :type => "<model type>",
                :id => "<model id>",
                :revision => "<model revision>",
                :visibility => "visible",
                :deploy => {},
                :outputs => [
                  {
                    :connections => [],
                    :name => "<output name>",
                    :type => "<output type>",
                    :visibility => "visible"
                  }
                ],
                :self => {
                  :connections => [],
                  :name => "self",
                  :type => "<model type>",
                  :visibility => "hidden"
                }
              },
              {
                :id => "<config id>",
                :revision => "<config revision>",
                :name => "<config name>",
                :type => "configuration",
                :inputs => [],
                :outputs => [],
                :visibility => "hidden",
                :self => {
                  :connections => [
                    {
                      :to => [
                        0,
                        0,
                        0
                      ]
                    }
                  ],
                  :name => "self",
                  :type => "configuration",
                  :visibility => "hidden"
                }
              }
            ],
            :center => {
              :x => 0,
              :y => 0
            }
          }
        end
      end
    end
  end
end
