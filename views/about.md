This is some [markdown](https://commonmark.org/help/) describing the site and anything you might wish to say.

![midas logo](/covid-lab/assets/midas.png)

You can add whatever things you find important here. Such as that image above and the things below.

## Another header

Something else.

## Third header

Something third.

### A listing

* foo
* bar
* baz

## Code

Maybe a code listing

```
Hello world.

foo bar
```

Any references to `functions` with simple code spans.

## Quotes

When you **need** to *refer* to what somebody else said.

> Blah -- a wise human
>
> Woof -- a wise dog

This is how you do it
