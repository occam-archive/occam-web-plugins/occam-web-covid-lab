# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../models/workflow.rb"

class Occam
  module Plugins
    module CovidLab
      module Controllers
        class CovidLabController < Occam::Controllers::ObjectController
          # This is the account token (keep it secret)
          def covid_account
            auth = {
              "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZGVudGl0eSI6IjZNdjhyRmJHNnVtN2NLZTdDMlJ2UllzTnU0SnQ3OURjNFhwa1lMSFd1c1NrazRZIiwicGVyc29uIjoiUW1adUM5N2pyWlNhWWFZbkVSVzNva1NoYWN1aGgzWGNXYm92eFhDRjdCbmZrcSJ9.WJxbdVN4eMoR3ycfyo8CS8BvLFLVn5r-pT48vITyNPo",
              "identity": "6Mv8rFbG6um7cKe7C2RvRYsNu4Jt79Dc4XpkYLHWusSkk4Y"
            }

            Occam::Account.new(auth)
          end

          # Get our views relative to the plugin source
          rootPath = File.realpath(File.join(File.dirname(__FILE__), ".."))
          set :slim, :views => File.join(rootPath, "views")

          # Use the layout.slim as the main source for content/structure of
          # the page. Everything rendered will be within the `yield` inside
          # this layout.slim file.
          set :slim, :layout => :layout

          # SASS support for our plugin
          require 'sass/plugin/rack'
          Sass::Plugin.options[:template_location] = File.join(rootPath, "stylesheets")
          Sass::Plugin.options[:css_location]      = "./public/stylesheets/plugins/covid-lab"
          use Sass::Plugin::Rack

          # Handle static assets
          get '/covid-lab/assets/*' do |path|
            send_file File.join(rootPath, "public", path)
          end

          # The landing page for everything we do
          get '/covid-lab/?' do
            # Render index.slim inside layout.slim
            render :slim, :index
          end

          # Just a page about what this whole thing is about
          get '/covid-lab/about/?' do
            render :slim, :about
          end

          # Get a list of models known to the system
          get '/covid-lab/models/?' do
            # Get all models, which are objects specifically tagged for our
            # purpose.

            # TODO: eventually, we will tag the models
            #result = Occam::Object.fullSearch(:tags => ["covid-lab-model"],
            #                                 :account => covid_account)

            # Just to test
            result = Occam::Object.fullSearch(:name => "", :type => "script", :account => covid_account)

            @models = result[:objects]

            render :slim, :models
          end

          # Get a listing of outputs for a particular model
          # :model -> the object id for a model
          get '/covid-lab/models/:model/?' do
            # Get the output of the provided model
            # TODO: We might also want any output of the plotter that used the
            # output as an input.

            # We want a listing of the output with columns for parameters
            @model = Occam::Object.new(:id => params[:model],
                                       :account => covid_account)
            @outputs = @model.generated(:configurations => true)[:outputs]

            render :slim, :model
          end

          # Get a listing of outputs for a particular model
          # :model -> the object id for a model
          get '/covid-lab/models/:model/outputs/?' do
            # Get the output of the provided model
            # TODO: We might also want any output of the plotter that used the
            # output as an input.

            # We want a listing of the output with columns for parameters
            @model = Occam::Object.new(:id => params[:model],
                                       :account => covid_account)
            @outputs = []

            render :slim, :outputs
          end

          # Craft and run an experiment to perform the given configuration on
          # the given model.
          # :model -> the object id for a model
          post '/covid-lab/models/:model/run/?' do
            # Get the model we are configuring and running.
            @model = Occam::Object.new(:id => params[:model],
                                       :account => covid_account)

            # Gather the configuration data in its standard form.
            # Occam allows params[:data] to be broken down into a Hash:
            configuration = Occam::Workflow.decode_base64(params[:data])

            # Determine a reference to the configuration schema
            schema = {
              :id => @model.id,
              :revision => @model.revision,
              :file => @model.info[:inputs][0][:schema]
            }

            configFile = @model.info[:inputs][0][:file] || "config.json"

            # Create the configuration object and get its hash
            configObject = Occam::Object.create(:name => "COVIDLab Configuration for #{@model.info[:name]}",
                                                :type => "configuration",
                                                :info => {
                                                  :schema => schema,
                                                  :file => configFile,
                                                  :tags => ["covid-lab-config"]
                                                },
                                                :account => covid_account)

            # Add the configuration file to it
            configObject = configObject.set(configuration.to_json, configFile, :type => "json")

            # Fork/craft an appropriate workflow using that hash
            # We can hand-craft a workflow for this purpose.
            workflow = Occam::Plugins::CovidLab::Workflow.template

            # Update model object in workflow
            info = @model.info
            workflow[:connections][0][:inputs][0][:name] = info[:inputs][0][:name]
            workflow[:connections][0][:outputs] = info[:outputs] || []
            workflow[:connections][0][:outputs].each do |output|
              output[:visibility] = "visible"
              output[:connections] = []
            end
            workflow[:connections][0][:name] = info[:name]
            workflow[:connections][0][:type] = info[:type]
            workflow[:connections][0][:self][:type] = info[:type]
            workflow[:connections][0][:id] = @model.id
            workflow[:connections][0][:revision] = @model.revision

            # Update configuration object in workflow
            info = configObject.info(true)
            workflow[:connections][1][:name] = info[:name]
            workflow[:connections][1][:id] = configObject.id
            workflow[:connections][1][:revision] = configObject.revision

            # Now we craft the experiment and place the workflow data inside
            workflowName = "COVIDLab Workflow for #{@model.info[:name]}"
            experiment = Occam::Object.create(:name => "COVIDLab Experiment for #{@model.info[:name]}",
                                              :type => "experiment",
                                              :info => {
                                                :includes => [
                                                  {
                                                    :name => workflowName,
                                                    :type => "workflow",
                                                    :file => "data.json"
                                                  }
                                                ],
                                                :contains => [
                                                  {
                                                    :name => workflowName,
                                                    :type => "workflow",
                                                    :summary => "Built via the COVIDLab component."
                                                  }
                                                ],
                                                :tags => ["covid-lab-experiment"]
                                              },
                                              :account => covid_account)

            # Add the workflow data file to it
            experiment = experiment.set(workflow.to_json, "data.json", :type => "json")

            # Queue that workflow and retain it via the hash
            experiment = experiment.as(Occam::Workflow)
            run = experiment.queue

            puts run
            run
          end

          # Retrieve a single output produced by a model
          # :model -> the object id for a model
          # :output -> the hash for the configuration
          get '/covid-lab/models/:model/outputs/:output/?' do
            # Just render the output and its provenance
            @model = Occam::Object.new(:id => params[:model],
                                       :account => covid_account)
            @output = Occam::Object.new(:id => params[:output],
                                        :account => covid_account)

            # Look up the workflow for that output hash, if any

            # Get the outputs for that workflow and render them

            render :slim, :output
          end
        end
      end
    end
  end

  use Plugins::CovidLab::Controllers::CovidLabController
end
